<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('title', 100);
            $table->string('pic', 255)->nullable();
            $table->integer('price')->default(1);
            $table->text('desc')->nullable();
            $table->timestamp('sell_at');
            $table->boolean('enabled')->default(true);
            $table->bigInteger('cgy_id');
            // $table->foreign('cgy_id')->reference('id')->on('cgys')->onDelete('cascade');
            $table->string('source', 100);
            $table->string('options', 200)->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
